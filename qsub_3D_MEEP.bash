#!/bin/bash
#PBS -M	sgeoffroygagnon@gmail.com
#PBS -m	abe
#Requests for 16 processors on any number of nodes, 2Gb per processor = 32Gb total
#PBS -l	procs=4
#PBS -l pmem=4gb
# estimated run time:
#PBS -l walltime=0:5:00:00
# the job is not restartable
#PBS -r n

echo "Node file: $PBS_NODEFILE :"
echo "---------------------"
cat $PBS_NODEFILE
echo "---------------------"

CORES=$PBS_NP
echo "Running on $CORES cores."

# loading MEEP module

module load meep/1.3-mpi-1

# cd to directory in which this *.pbs file is located
cd $PBS_O_WORKDIR

echo "Current working directory is `pwd`" 
echo "Starting run at: `date`"
mpiexec meep-mpi doubleSphere3D.ctl > outputDoubleSphere3D.${PBS_JOBID}.out

echo "Job finished with exit code $? at: `date`"


