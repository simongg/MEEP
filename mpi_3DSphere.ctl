;3D simulation of coupled spheres
(set! eps-averaging? false)

(define n 1.83)
(define rad 3)
(define dpml 2)
(define time 3)

(define y (+ (* 2 rad) (* 2 dpml)))
(define x (+ (* 4 rad) (* 4 dpml) 4))
(define z (+ (* 2 rad) (* 2 dpml)))

(set! symmetries (list (make mirror-sym (direction Y)) (make mirror-sym (direction Z))))

(set! geometry-lattice (make lattice (size x y z)))

(set! geometry (list (make sphere(center rad 0 0)(radius rad)
	(material (make dielectric (epsilon (* n n)))))
	(make sphere(center (* -1 rad) 0)(radius rad)
	(material (make dielectric (epsilon (* n n)))))))

(define-param sigma1 (/ (- rad) 2.2))
(define (my-amp-func p) 
	(exp (- (/ (+ (sqr (vector3-x p)) (sqr (vector3-y p))) 
		(* 2(sqr sigma1)))
		)
	)
) 
; Gaussian approximation to the spatial field in SMF


(define sourceY (* (- y dpml dpml) 1 ))
(define sourceZ (* (- z dpml dpml) 1 ))
(set! sources (list
                (make source
                (src (make continuous-src (frequency 1)(width 10)))
                (component Ez) (size no-size sourceY sourceZ)(amplitude 1)
                (amp-func my-amp-func)
                (center (+ (/ x -2) dpml) 0 0)
                )
            )
)
							
(set! pml-layers (list	(make pml (thickness dpml) (direction X))
						(make pml (thickness dpml) (direction Y))
						(make pml (thickness dpml) (direction Z))))
					
(set! resolution 25)

(use-output-directory)
(run-until time 
	(after-time (- time 1) (in-volume (volume (center 25 0 0) (size no-size y z)) output-efield-z)
	)
)
