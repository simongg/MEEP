;3D circle first test with fully working gaussian beam

(reset-meep)
(set! eps-averaging? false)

(define time 200) ; time steps in the simulation
(define-param n 1.83) ; index of spheres
(define-param rad 3) ; radius of spheres
(define-param pad 1) ; padding between sphere and edge of PML
(define-param dpml 2) ; thickness of PML

; lattice dimension
(define sx (* (+ (* rad 2) pad dpml) 2)) ; x size (cell is from -x/2 to x/2)
(define sy (* (+ rad pad dpml) 2)) ; x size (cell is from -y/2 to y/2)
(define sz (* (+ rad pad dpml) 2)) ; x size (cell is from -z/2 to z/2)

; set lattice
(set! geometry-lattice (make lattice (size sx sy sz))) ;(x, y, z)

; set up sphere geometry
;(set! geometry (list (make sphere(center rad 0 0)(radius rad)
;	(material (make dielectric (epsilon (* n n)))))
;	(make sphere(center (* -1 rad) 0)(radius rad)
;	(material (make dielectric (epsilon (* n n)))))))

; define our gaussian function in the y and z direction	
(define-param sigma1 (/ rad 2.2)) ; sigma1 needs to be slightly smaller than half of the radius  
(define (gaussYZ p) 
	(exp (- (/ (+ (sqr (vector3-z p)) (sqr (vector3-y p))) 
		(* 2 (sqr sigma1)))
		)
	)
)

; Set source
(define sourceZ sz)
(define sourceY sy)
(set! sources (list
               (make source
				(src (make continuous-src (frequency 1)(width 5)))
                (component Ez) (size no-size sy sz)(amplitude 2)
				(amp-func gaussYZ)
                (center (+ (/ sx -2) dpml) 0 0))))
				
; define pml layer				
(set! pml-layers (list	(make pml (thickness dpml) (direction X))
						(make pml (thickness dpml) (direction Y))
						(make pml (thickness dpml) (direction Z))))
					
(set! resolution 15)

(use-output-directory)

(run-until time 
	(after-time (- time 3 ) combine-step-funcs
		(in-volume (volume (center 0 0 0) (size sx sy no-size)) 
			(to-appended "efield-xy" output-efield-z))
		(in-volume (volume (center (+ (/ sx -2) dpml dpml) 0 0) (size no-size sy sz))
			(to-appended "efield-yz" output-efield-z))
	)
)
	